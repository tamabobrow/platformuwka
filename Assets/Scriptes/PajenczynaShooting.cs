﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PajenczynaShooting : MonoBehaviour
{
    public Transform celownik;
    public Camera cam;
    Vector2 mousePos;
    public float radius = 5f;
    public GameObject kulkaZPajenczynom;
    private bool OneShot = true;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }
    private void FixedUpdate()
    {
        Vector2 celPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 lookDir = radius*mousePos.normalized + celPos;
        celownik.position = new Vector3(lookDir.x,lookDir.y);
        

    }

    void Shoot()
    {
        if(OneShot)
        {
            Instantiate(kulkaZPajenczynom, transform.position, transform.rotation);
            OneShot = !OneShot;
        }
    }
}
