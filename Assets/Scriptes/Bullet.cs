﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private GameObject celownik;
    public float speed = 50f;
    private Rigidbody2D rigid;
    public HingeJoint2D hook;
    public float distance = 5f;
    public float mnoznik = 10f;
    private Transform startingPoint;
    private GameObject lastElement;
    public GameObject linkPrefab;
    Rigidbody2D poprzedniElement;
    bool spawner = true;
    public int link = 0;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        celownik = GameObject.FindGameObjectWithTag("Celownik");
        Vector2 vec = celownik.transform.position - transform.position;
        rigid.velocity = vec * speed * Time.deltaTime;
        Debug.Log(celownik.transform.position);
        startingPoint = GameObject.FindGameObjectWithTag("PlayerPoint").transform;
        poprzedniElement = GetComponent<Rigidbody2D>();
        lastElement = gameObject;
        GenerateLink();
    }

    // Update is called once per frame
    void Update()
    {


        if (Vector2.Distance(startingPoint.position, transform.position) >= distance * link / mnoznik && spawner)
        {
            GenerateLink();
            Debug.Log("generate");
        }
    }

    void GenerateLink()
    {
        link++;
        lastElement = Instantiate(linkPrefab, startingPoint);
        Debug.Log("inicjacja " + link);
        hook = lastElement.GetComponent<HingeJoint2D>();
        hook.connectedBody = poprzedniElement;
        poprzedniElement = lastElement.GetComponent<Rigidbody2D>();
    }

    void GenerateLast()
    {

        GameObject.FindGameObjectWithTag("PlayerPoint").GetComponent<HingeJoint2D>().connectedBody = poprzedniElement;

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        spawner = false;
        rigid.velocity = new Vector2(0, 0);
        Debug.Log(collision.name);
        GenerateLast();
    }
}
