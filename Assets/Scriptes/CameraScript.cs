﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform target;
    public float oddalenie = 10f;
    void LateUpdate()
    {
        transform.position = new Vector3(target.position.x,target.position.y, oddalenie * -1);
    }
}
